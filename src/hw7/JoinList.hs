{-# LANGUAGE FlexibleInstances, TypeSynonymInstances #-}

module JoinList where

import Data.Monoid
import Sized
import Scrabble
import Buffer

data JoinList m a = Empty
                  | Single m a
                  | Append m (JoinList m a) (JoinList m a)
  deriving (Eq, Show)


----------------
-- Exercise 1 --
----------------

(+++) :: Monoid m => JoinList m a -> JoinList m a -> JoinList m a
(+++) jla jlb = Append ((tag jla) <> (tag jlb)) jla jlb

tag :: Monoid m => JoinList m a -> m
tag (Empty)        = mempty
tag (Single m _)   = m
tag (Append m _ _) = m


----------------
-- Exercise 2 --
----------------

nodeSize :: (Sized b, Monoid b) => JoinList b a -> Int
nodeSize n = getSize $ size $ tag n

indexJ :: (Sized b, Monoid b) => Int -> JoinList b a -> Maybe a
indexJ _ Empty        = Nothing
indexJ 0 (Single _ a) = Just a
indexJ _ (Single _ a) = Nothing
indexJ n node@(Append _ jll jlr)
  | n >= nodeSize node = Nothing
  | n >= nodeSize jll  = indexJ (n - nodeSize jll) jlr
  | otherwise          = indexJ n jll

dropJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
dropJ n jl | n <= 0           = jl
dropJ n jl | n >= nodeSize jl = Empty
dropJ n (Append _ jll jlr)
  | n >= nodeSize jll = dropJ (n - nodeSize jll) jlr
  | otherwise         = (dropJ n jll) +++ jlr

takeJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
takeJ n _ | n <= 0            = Empty
takeJ n jl | n >= nodeSize jl = jl
takeJ n (Append _ jll jlr)
  | n >= nodeSize jll = jll +++ (takeJ (n - nodeSize jll) jlr)
  | otherwise         = takeJ n jll


----------------
-- Exercise 3 --
----------------

scoreLine :: String -> JoinList Score String
scoreLine str = Single (scoreString str) str


----------------
-- Exercise 4 --
----------------

instance Buffer (JoinList (Score, Size) String) where
  toString Empty              = []
  toString (Single _ s)       = s
  toString (Append _ jll jlr) = toString jll ++ toString jlr

  fromString str              = toBuffer $ lines str
    where toBuffer []         = Empty
          toBuffer (s:ss)     = Single (scoreString s, Size 1) s +++ toBuffer ss

  -- Uses normal indices PLUS one due to presence of 0th line from initializer buffer
  replaceLine n s jl          = takeJ n jl +++ fromString s +++ dropJ (n+1) jl

  line                        = indexJ

  numLines jl                 = getSize $ size $ tag jl

  value jl                    = getScore $ fst $ tag jl
