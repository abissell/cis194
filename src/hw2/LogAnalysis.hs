{-# OPTIONS_GHC -Wall #-}
module LogAnalysis where

import Log


----------------
-- Exercise 1 --
----------------

parseMessage :: String -> LogMessage
parseMessage [] = Unknown ""
parseMessage msg = parseTypeAndWords (words msg)

parseTypeAndWords :: [String] -> LogMessage
parseTypeAndWords ("E":wrds) = parseWords ["E"] wrds
parseTypeAndWords ("I":wrds) = parseWords ["I"] wrds
parseTypeAndWords ("W":wrds) = parseWords ["W"] wrds
parseTypeAndWords wrds = Unknown (unwords wrds)

parseWords :: [String] -> [String] -> LogMessage
parseWords ["E"] wrds = case readLeadingInt wrds of
                          Failure -> Unknown (unwords ("E":wrds))
                          OK i -> parseWordsAfterType ("E":[(head wrds)]) (Error i) (drop 1 wrds)
parseWords ["I"] wrds = parseWordsAfterType ["I"] Info wrds
parseWords ["W"] wrds = parseWordsAfterType ["W"] Warning wrds
parseWords pre wrds = Unknown ((unwords pre) ++ (unwords wrds))

parseWordsAfterType :: [String] -> MessageType -> [String] -> LogMessage
parseWordsAfterType pre t wrds = case readLeadingInt wrds of
                                   Failure -> Unknown ((unwords pre) ++ (unwords wrds))
                                   OK i -> LogMessage t i (unwords (drop 1 wrds))

data LeadingInt = Failure
                | OK Int

readLeadingInt :: [String] -> LeadingInt
readLeadingInt [] = Failure
readLeadingInt (x:_) = case reads x of
                          [(i,"")] -> OK i
                          _ -> Failure

parse :: String -> [LogMessage]
parse file = parseLines (lines file) []

parseLines :: [String] -> [LogMessage] -> [LogMessage]
parseLines [] msgs = msgs;
parseLines xs msgs = parseMessage (head xs) : parseLines (drop 1 xs) msgs


----------------
-- Exercise 2 --
----------------

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) tree = tree
insert m@(LogMessage _ _ _) Leaf = Node Leaf m Leaf
insert m@(LogMessage _ ts _) (Node lt tm@(LogMessage _ nodeTs _) rt)
  | ts <= nodeTs = Node (insert m lt) tm rt
  | otherwise    = Node lt tm (insert m rt)
insert _ (Node _ (Unknown _) _) = Leaf -- should be impossible, no Unknown nodes created


----------------
-- Exercise 3 --
----------------

build :: [LogMessage] -> MessageTree
build [] = Leaf
build (x:xs) = insert x (build xs)


----------------
-- Exercise 4 --
----------------

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node lt m rt) = inOrder lt ++ [m] ++ inOrder rt


----------------
-- Exercise 5 --
----------------

sortLogMessages :: [LogMessage] -> [LogMessage]
sortLogMessages xs = inOrder (build xs)

filterForSevereErrors :: Int -> [LogMessage] -> [LogMessage]
filterForSevereErrors _ [] = []
filterForSevereErrors level (m@(LogMessage (Error i) _ _):xs)
  | i >= level = m : filterForSevereErrors level xs
  | otherwise  = filterForSevereErrors level xs
filterForSevereErrors level (_:xs) = filterForSevereErrors level xs

extractStrings :: [LogMessage] -> [String]
extractStrings [] = []
extractStrings ((LogMessage _ _ str):xs) = str : extractStrings xs
extractStrings ((Unknown _):xs) = extractStrings xs

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong xs = extractStrings (filterForSevereErrors 50 (sortLogMessages xs))
