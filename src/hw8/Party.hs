{-# OPTIONS_GHC -fno-warn-orphans #-}

module Party where

import Data.Monoid
import Data.Tree
import Data.List
import Employee
import System.IO


----------------
-- Exercise 1 --
----------------

glCons :: Employee -> GuestList -> GuestList
glCons emp@(Emp {empFun = fun}) (GL emps glFun) = GL (emp:emps) (fun+glFun)

instance Monoid GuestList where
  mempty                              = GL [] 0
  mappend (GL emps1 f1) (GL emps2 f2) = GL (emps1 ++ emps2) (f1+f2)

moreFun :: GuestList -> GuestList -> GuestList
moreFun a b
  | getFun a >= getFun b = a
  | otherwise = b

getFun :: GuestList -> Fun
getFun (GL _ fun) = fun


----------------
-- Exercise 2 --
----------------

treeFold :: b -> (b -> Tree a -> b) -> Tree a -> b
treeFold e f tree@(Node {subForest=xs}) = foldl' f (f e tree) xs

tag :: Tree a -> a
tag (Node {rootLabel=a}) = a

treeSum = treeFold 0 (\x t -> x + tag t)


----------------
-- Exercise 3 --
----------------

nextLevel :: Employee -> [(GuestList, GuestList)] -> (GuestList, GuestList)
nextLevel emp gls = addEmp $ mconcat $ map bestSubs gls
    where bestSubs (gla,glb)  = (glb, moreFun gla glb)
          addEmp (gla,glb)    = (glCons emp gla, glb)


----------------
-- Exercise 4 --
----------------

maxFun :: Tree Employee -> GuestList
maxFun t = moreFun (fst $ findOptGls t) (snd $ findOptGls t)

findOptGls :: Tree Employee -> (GuestList, GuestList)
findOptGls (Node emp []) = (glCons emp mempty, mempty)
findOptGls (Node emp subs) = nextLevel emp (map findOptGls subs)


----------------
-- Exercise 5 --
----------------

main :: IO ()
main = do
    contents <- readFile "company.txt"
    putStrLn (printGuestList contents)

printGuestList :: String -> String
printGuestList s =
    let (GL emps fun) = maxFun $ read s
        result        = unlines $ [show fun] ++ (sort $ map empName emps)
    in result
