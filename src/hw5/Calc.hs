{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wall #-}

import ExprT
import Parser
import qualified StackVM as SVM


----------------
-- Exercise 1 --
----------------

eval :: ExprT -> Integer
eval (Mul x y) = eval x * eval y
eval (Add x y) = eval x + eval y
eval (Lit x)   = x


----------------
-- Exercise 2 --
----------------

evalStr :: String -> Maybe Integer
evalStr s = case parseExp Lit Add Mul s of
              Just ex -> Just (eval ex)
              Nothing -> Nothing


----------------
-- Exercise 3 --
----------------

class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a

instance Expr ExprT where
  lit x   = Lit x
  add x y = Add x y
  mul x y = Mul x y

reify :: ExprT -> ExprT
reify = id


----------------
-- Exercise 4 --
----------------

instance Expr Integer where
  lit x   = x
  add x y = (+) x y
  mul x y = (*) x y

instance Expr Bool where
  lit x   = x > 0
  add x y = x || y
  mul x y = x && y

newtype MinMax  = MinMax Integer deriving (Eq, Show)
instance Expr MinMax where
  lit x                     = MinMax x
  add (MinMax x) (MinMax y) = MinMax $ max x y
  mul (MinMax x) (MinMax y) = MinMax $ min x y

newtype Mod7 = Mod7 Integer deriving (Eq, Show)
instance Expr Mod7 where
  lit x                 = Mod7 $ x `mod` 7
  add (Mod7 x) (Mod7 y) = Mod7 $ (x + y) `mod` 7
  mul (Mod7 x) (Mod7 y) = Mod7 $ (x * y) `mod` 7

testExp :: Expr a => Maybe a
testExp = parseExp lit add mul "(3 * -4) + 5"

testInteger = testExp :: Maybe Integer
testBool    = testExp :: Maybe Bool
testMM      = testExp :: Maybe MinMax
testSat     = testExp :: Maybe Mod7


----------------
-- Exercise 5 --
----------------

instance Expr SVM.Program where
  lit x   = [SVM.PushI x]
  add x y = x ++ y ++ [SVM.Add]
  mul x y = x ++ y ++ [SVM.Mul]

compile :: String -> Maybe SVM.Program
compile s = parseExp lit add mul s
