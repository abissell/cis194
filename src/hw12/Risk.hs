{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Risk where

import Control.Monad
import Control.Monad.Random
import Data.Functor.Identity
import Data.List

------------------------------------------------------------
-- Die values

newtype DieValue = DV { unDV :: Int } 
  deriving (Eq, Ord, Show, Num)

first :: (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)

instance Random DieValue where
  random           = first DV . randomR (1,6)
  randomR (low,hi) = first DV . randomR (max 1 (unDV low), min 6 (unDV hi))

die :: Rand StdGen DieValue
die = getRandom

------------------------------------------------------------
-- Risk

type Army = Int

data Battlefield = Battlefield { attackers :: Army, defenders :: Army }


----------------
-- Exercise 2 --
----------------

battle :: Battlefield -> Rand StdGen Battlefield
battle bf =
  replicateM att die >>= \attDice ->
  replicateM def die >>= \defDice ->
  return (tally bf attDice defDice)
    where att = min 3 (max 0 (attackers bf - 1))
          def = min 2 (defenders bf)

tally :: Battlefield -> [DieValue] -> [DieValue] -> Battlefield
tally bf a d
  | null a || null d = bf
  | otherwise        = tally' bf $ zip (reverse $ sort a) (reverse $ sort d)

tally' :: Battlefield -> [(DieValue, DieValue)] -> Battlefield
tally' bf ds = Battlefield (attackers bf - fst lost) (defenders bf - snd lost)
  where lost = foldl (\(ta, td) (aDv, dDv) -> if aDv > dDv then (ta, td+1) else (ta+1, td)) (0,0) ds


----------------
-- Exercise 3 --
----------------

invade :: Battlefield -> Rand StdGen Battlefield
invade bf =
  battle bf >>= \outcome ->
  if (isDone outcome) then return outcome else invade outcome
    where isDone bf = attackers bf < 2 || defenders bf <= 0


----------------
-- Exercise 4 --
----------------

successProb :: Battlefield -> Rand StdGen Double
successProb bf =
  outcomeList 10000 bf >>= \outcomes ->
  return $ (fromIntegral $ sum (map fst outcomes)) / 10000

outcomeList :: Int -> Battlefield -> Rand StdGen [(Int, Int)]
outcomeList n bf = replicateM n (successEval bf)
  where successEval bf = liftM (\bf -> if defenders bf <= 0 then (1,0) else (0,1)) $ invade bf

displaySuccessProb bf = do
  prob <- evalRandIO (successProb bf)
  putStrLn ("prob: " ++ show prob)
