----------------
-- Exercise 1 --
----------------

first :: (a -> b) -> (a,c) -> (b,c)
first f (a,c) = (f a,c)

instance Functor Parser where
  fmap f p = Parser g
    where g = fmap (first f) . (runParser p)


----------------
-- Exercise 2 --
----------------

instance Applicative Parser where
  pure a    = Parser (\s -> Just(a, s))
  p1 <*> p2 = Parser f
    where
      f s = case ((runParser p1) s) of
              Nothing      -> Nothing
              Just (g, rs) -> case ((runParser p2) rs) of
                                Nothing       -> Nothing
                                Just (b, rrs) -> Just (g b, rrs)


----------------
-- Exercise 3 --
----------------

abParser :: Parser (Char, Char)
abParser = (,) <$> (char 'a') <*> (char 'b')

abParser_ :: Parser ()
abParser_ = (const ()) <$> abParser

intPair :: Parser [Integer]
intPair = (\(a,_,c) -> [a,c]) <$> pairParser
    where pairParser = (,,) <$> posInt <*> (char ' ') <*> posInt


----------------
-- Exercise 4 --
----------------

instance Alternative Parser where
  empty     = Parser (const Nothing)
  p1 <|> p2 = Parser f
    where
      f s = case ((runParser p1) s) of
              Just (a, rs) -> Just (a, rs)
              Nothing      -> case ((runParser p2) s) of
                                Nothing      -> Nothing
                                Just (b, rs) -> Just (b, rs)


----------------
-- Exercise 5 --
----------------

intOrUppercase :: Parser ()
intOrUppercase = (const ()) <$> posInt <|> uppercase
  where uppercase = (const ()) <$> satisfy isUpper
