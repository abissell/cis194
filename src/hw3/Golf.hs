{-# OPTIONS_GHC -Wall #-}

module Golf where


----------------
-- Exercise 1 --
----------------

skips :: [a] -> [[a]]
skips xs = map (map fst) $ map f $ zip indices $ replicate (length xs) $ zip xs indices
    where indices = [1..length(xs)]
          f (n,elems) = filter (g n) elems
          g n = h
              where h (_,x) = x `mod` n == 0


----------------
-- Exercise 2 --
----------------

localMaxima :: [Int] -> [Int]
localMaxima xs = map mid $ filter maxima (zip3 xs (tail xs) (tail (tail xs)))
    where maxima (x,y,z) = x < y && y > z
          mid (_,a,_)    = a


----------------
-- Exercise 3 --
----------------

histogram :: [Int] -> String
histogram ns = getHistoLines (maximum $ getCounts ns) (getCounts ns)
                      ++ "==========\n0123456789\n"

getHistoLines :: Int -> [Int] -> String
getHistoLines 0 _  = []
getHistoLines n xs = getHistoLine n xs ++ getHistoLines (n-1) xs

getHistoLine :: Int -> [Int] -> String
getHistoLine n xs = (map f xs) ++ ['\n']
    where f num
            | num >= n  = '*'
            | otherwise = ' '

getCounts :: [Int] -> [Int]
getCounts ns = map (count ns) [0..9]
    where count [] _ = 0
          count (x:xs) n = (if x == n then 1 else 0) + count xs n
