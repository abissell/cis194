----------------
-- Exercise 1 --
----------------

-- Generate a list of digits in an Integer from that Integer
toDigits :: Integer -> [Integer]
toDigits n = reverse (toDigitsRev n)

-- Generate a reversed list of digits in an Integer from that Integer
toDigitsRev :: Integer -> [Integer]
toDigitsRev n
  | n <= 0          = []
  | n `div` 10 == 0 = [n]
  | otherwise       = n `mod` 10 : toDigitsRev (n `div` 10)


----------------
-- Exercise 2 --
----------------

doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther xs = reverse (doubleEveryOtherFwd (reverse xs))

doubleEveryOtherFwd :: [Integer] -> [Integer]
doubleEveryOtherFwd []         = []
doubleEveryOtherFwd (x:[])     = [x]
doubleEveryOtherFwd (x:(y:zs)) = x : (2*y) : doubleEveryOtherFwd zs


----------------
-- Exercise 3 --
----------------

sumDigits :: [Integer] -> Integer
sumDigits []       = 0
sumDigits (x:xs)   = sum (toDigits x) + sumDigits xs


----------------
-- Exercise 4 --
----------------

validate :: Integer -> Bool
validate n = (sumDigits (doubleEveryOther (toDigits n))) `mod` 10 == 0


----------------
-- Exercise 5 --
----------------

type Peg = String
type Move = (Peg, Peg)

hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi 0 from to storage = []
hanoi 1 from to storage = [(from, to)]
hanoi n from to storage = hanoi (n-1) from storage to ++ [(from, to)] ++ hanoi (n-1) storage to from
