{-# OPTIONS_GHC -Wall #-}


----------------
-- Exercise 1 --
----------------

fun1' :: [Integer] -> Integer
fun1' = product . map (subtract 2) . filter even

fun2' :: Integer -> Integer
fun2' = sum . filter even . takeWhile (/=1)
        . iterate (\n -> if even n then n `div` 2 else 3 * n + 1)


----------------
-- Exercise 2 --
----------------

data Tree a = Leaf
            | Node Integer (Tree a) a (Tree a)
  deriving (Show, Eq)

foldTree :: [a] -> Tree a
foldTree = foldr addNode Leaf

addNode :: a -> Tree a -> Tree a
addNode a Leaf = Node 0 Leaf a Leaf
addNode a (Node n lt a' rt)
  | (depth lt <= depth rt) = (Node (newDepth (addNode a lt)) (addNode a lt) a' rt)
  | otherwise              = (Node (newDepth (addNode a rt)) lt a' (addNode a rt))
    where newDepth newSubTree = if (n == depth newSubTree) then n+1 else n

depth :: Tree a -> Integer
depth Leaf = -1
depth (Node n _ _ _) = n


----------------
-- Exercise 3 --
----------------

xor :: [Bool] -> Bool
xor = foldl (\x y -> (x || y) && not (x && y)) False

map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\x ys -> (f x) : ys) []

myFoldl :: (a -> b -> a) -> a -> [b] -> a
myFoldl f base xs = foldr g base (reverse xs)
    where g x b = b `f` x


----------------
-- Exercise 4 --
----------------

sieveSundaram :: Integer -> [Integer]
sieveSundaram n = map (\x -> 2*x + 1) $ filter sieve [1..n]
    where sieve x = (notElem x $ map (\(i,j) -> i + j + 2*i*j) $ cartProd [1..n] [1..n])

cartProd :: [a] -> [b] -> [(a,b)]
cartProd xs ys = [(x,y) | x <- xs, y <- ys]
