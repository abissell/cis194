{-# LANGUAGE FlexibleInstances #-}

import Data.List


----------------
-- Exercise 1 --
----------------

fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

fibs1 :: [Integer]
fibs1 = map fib [0..]


----------------
-- Exercise 2 --
----------------

fibs2 :: [Integer]
fibs2 = map (\(x,y) -> x+y) $ scanl acc (0,0) [0..]
    where acc (_,_) 0 = (0,1)
          acc (_,_) 1 = (0,1)
          acc (x,y) _ = (y,x+y)


----------------
-- Exercise 3 --
----------------

data Stream a = Stream a (Stream a)

streamToList :: Stream a -> [a]
streamToList (Stream x subStream) = x : streamToList subStream

instance Show a => Show (Stream a) where
  show stream = show $ take 40 $ streamToList stream


----------------
-- Exercise 4 --
----------------

streamRepeat :: a -> Stream a
streamRepeat x = (Stream x (streamRepeat x))

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Stream a subStream) = (Stream (f a) (streamMap f subStream))

streamFromSeed :: (a -> a) -> a -> Stream a
streamFromSeed f x = (Stream x (streamFromSeed f (f x)))


----------------
-- Exercise 5 --
----------------

nats :: Stream Integer
nats = streamFromSeed (+1) 0

interleaveStreams :: Stream a -> Stream a -> Stream a
-- this causes the ruler function not to terminate, thanks Sun Chao for an alternative impl
-- interleaveStreams (Stream x subStreamX) (Stream y subStreamY) = (Stream x (Stream y (interleaveStreams subStreamX subStreamY)))
interleaveStreams (Stream x ax) ay = (Stream x (interleaveStreams ay ax))

-- baseRulerStream :: Stream Integer
-- baseRulerStream = interleaveStreams zeros $ interleaveStreams ones $ interleaveStreams twos threeUps
--     where zeros    = streamRepeat 0
--           ones     = streamRepeat 1
--           twos     = streamRepeat 2
--           threeUps = streamMap (+3) nats

ruler :: Stream Integer
ruler = baseRuler 0
    where baseRuler n = interleaveStreams (streamRepeat n) $ baseRuler (n+1)


----------------
-- Exercise 6 --
----------------

x :: Stream Integer
x = (Stream 0 (Stream 1 (streamRepeat 0)))

streamVal :: Integer -> Stream Integer
streamVal n = (Stream n $ streamRepeat 0)

instance Num (Stream Integer) where
  fromInteger n = (Stream n (streamRepeat 0))
  negate (Stream n subStream) = (Stream (-1*n) (negate subStream))
  (+) (Stream x subX) (Stream y subY) = (Stream (x+y) (subX + subY))
  (*) (Stream x subX) (Stream y subY) = (Stream (x*y) tailStream)
    where tailStream = ((streamVal x) *subY) + (subX * (Stream y subY))

instance Fractional (Stream Integer) where
  (/) ax@(Stream x subX) bx@(Stream y subY) = (Stream (x `div` y) tailStream)
    where tailStream = (streamVal (1 `div` y)) * (subX - ((ax/bx)*subY))

fibs3 :: Stream Integer
fibs3 = x / (1 - x - x^2)


----------------
-- Exercise 7 --
----------------

data Matrix = Matrix Integer Integer Integer Integer

instance Show Matrix where
  show (Matrix a b c d) = "[" ++ (show a) ++ " " ++ (show b) ++ "\n " ++ (show c) ++ " " ++ (show d) ++ "]"

instance Num Matrix where
  (*) (Matrix a b c d) (Matrix e f g h) = (Matrix (a*e + b*g) (a*f + b*h) (c*e + d*g) (c*f + d*h))

f :: Matrix
f = Matrix 1 1 1 0

fib4 :: Integer -> Integer
fib4 0 = 0
fib4 n = fibNum $ f^n
  where fibNum (Matrix _ b _ _) = b
